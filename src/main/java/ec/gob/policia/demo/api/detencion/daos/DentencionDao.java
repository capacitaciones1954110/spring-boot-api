package ec.gob.policia.demo.api.detencion.daos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import ec.gob.policia.demo.api.detencion.dtos.DetencionDto;
import ec.gob.policia.demo.api.detencion.mappers.DetencionRowMapper;

@Component
public class DentencionDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<DetencionDto> getAll() {
        String query = "Select * from detencion";
        return jdbcTemplate.query(query, new DetencionRowMapper());
    }

    public List<DetencionDto> getByCedula(String cedula) {
        String query = "Select * from detencion where cedula_persona = ?";
        return jdbcTemplate.query(query, new DetencionRowMapper(), cedula);
    }

    public List<DetencionDto> getByCedulaPs(String cedula) {
        String query = "Select * from detencion where cedula_persona = ?";
        return jdbcTemplate.query(query, ps->{
            ps.setString(1, cedula);
        } ,new DetencionRowMapper());
    }

    public List<DetencionDto> getByCedulaNamedParemeter(String cedula) {
        String query = "Select * from detencion where cedula_persona =:cedula" ;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cedula", "1715310809");
        return namedParameterJdbcTemplate.query(query, params, new DetencionRowMapper());
    }
}
