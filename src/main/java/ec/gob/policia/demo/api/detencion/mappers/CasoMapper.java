package ec.gob.policia.demo.api.detencion.mappers;

import org.springframework.stereotype.Component;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;
import ec.gob.policia.demo.api.detencion.entities.Caso;

@Component
public class CasoMapper {
    public Caso toEntity(CasoCommand command){
        Caso entidad = new Caso();
        entidad.setNombreCaso(command.getNombreCaso());
        entidad.setNumeroCaso(command.getNumeroCaso());
        return entidad;
    }
}
