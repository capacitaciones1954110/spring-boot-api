package ec.gob.policia.demo.api.detencion.controllers.impl;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import ec.gob.policia.demo.api.detencion.controllers.CrearCasoCompletoController;
import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCompletoCommand;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarCasoCompletoUseCase;
import ec.gob.policia.demo.api.utils.ResponseGenerico;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CrearCasoCompletoControllerImpl implements CrearCasoCompletoController {

    private final AgregarCasoCompletoUseCase agregarCasoCompletoUseCase;

    public CrearCasoCompletoControllerImpl(AgregarCasoCompletoUseCase agregarCasoCompletoUseCase) {
        this.agregarCasoCompletoUseCase = agregarCasoCompletoUseCase;
    }

    @Override
    public ResponseEntity<ResponseGenerico> post(@Valid CasoCompletoCommand command) {
        log.info("Agregando caso completo {}", command);
        ResponseGenerico responseGenerico = new ResponseGenerico();
        try{
            agregarCasoCompletoUseCase.agregarCasoCompleto(command);
            responseGenerico.setData("Agregado");
            return ResponseEntity.ok(responseGenerico);
        }catch(Exception ex){
            responseGenerico.setMensaje(ex.getMessage());
            log.error("CREAR CASO: "+ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseGenerico);
        }
       
    }

}
