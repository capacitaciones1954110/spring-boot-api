package ec.gob.policia.demo.api.detencion.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.gob.policia.demo.api.detencion.controllers.CrearDetencionController;
import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarDetencionUseCase;
import ec.gob.policia.demo.api.utils.ResponseGenerico;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/siipne3w")
@Slf4j
public class CrearDetencionControllerImpl implements CrearDetencionController {

    private final AgregarDetencionUseCase agregarDetencionUseCase;

    public CrearDetencionControllerImpl(AgregarDetencionUseCase agregarDetencionUseCase) {
        this.agregarDetencionUseCase = agregarDetencionUseCase;
    }

    @Override
    public ResponseEntity<ResponseGenerico> get() {
        String saludo = "Hello World reload";
        ResponseGenerico responseGenerico = new ResponseGenerico();
        responseGenerico.setData(saludo);
        responseGenerico.setCodigo("200");
        responseGenerico.setMensaje("saludo");
        return ResponseEntity.ok(responseGenerico);
    }

    @Override
    public ResponseEntity<ResponseGenerico> post(DetencionCommand command) {
        log.info("Agregando detencion {}", command);
        ResponseGenerico responseGenerico = new ResponseGenerico();
        agregarDetencionUseCase.agregar(command);
        responseGenerico.setData("Registro agregado");
        responseGenerico.setCodigo("200");
        responseGenerico.setMensaje("Registro agregado");
        return ResponseEntity.ok(responseGenerico);
    }
}
