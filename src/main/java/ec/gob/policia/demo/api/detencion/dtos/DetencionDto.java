package ec.gob.policia.demo.api.detencion.dtos;

import lombok.Data;

@Data
public class DetencionDto {
    private String cedulaPersona;
    private String detalle;
}
