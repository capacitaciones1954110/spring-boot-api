package ec.gob.policia.demo.api.detencion.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.gob.policia.demo.api.detencion.entities.Detencion;

public interface DetencionRepository extends JpaRepository<Detencion, Integer> {
    
    List<Detencion> findByCedulaPersona(String cedulaPersona);

    @Query("select d from Detencion d where d.id = :id")
    Detencion getById(int id);

    @Query(nativeQuery = true, value = "select * from detencion")
    Detencion getAllNative();
}