package ec.gob.policia.demo.api.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import ec.gob.policia.demo.api.utils.ResponseGenerico;
import jakarta.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleValidationExceptions(HttpServletRequest request,
            MethodArgumentNotValidException ex) {
        ResponseGenerico responseGenerico = new ResponseGenerico();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            responseGenerico.setMensaje(fieldName + ": " + errorMessage);
            responseGenerico.setCodigo("400");
        });
        return ResponseEntity.badRequest().body(responseGenerico);
    }
}
