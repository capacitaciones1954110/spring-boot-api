package ec.gob.policia.demo.api.detencion.servicios.usecases;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCompletoCommand;

@FunctionalInterface
public interface AgregarCasoCompletoUseCase {

    void agregarCasoCompleto(CasoCompletoCommand command) throws Exception;
    
}
