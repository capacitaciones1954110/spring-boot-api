package ec.gob.policia.demo.api.detencion.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCompletoCommand;
import ec.gob.policia.demo.api.utils.ResponseGenerico;
import jakarta.validation.Valid;

public interface CrearCasoCompletoController {

    @PostMapping("/v1/casos-completo")
    ResponseEntity<ResponseGenerico> post(@Valid @RequestBody CasoCompletoCommand command);
}
