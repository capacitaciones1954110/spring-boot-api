package ec.gob.policia.demo.api.utils;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import lombok.Data;

@Data
public class Criteria {
    private List<Condiciones> condiciones;

    public String getCondiciones() {
        String whereCondition = "";
        int contador = 1;
        for (Condiciones tmp : condiciones) {
            if (contador < condiciones.size()) {
                whereCondition = whereCondition + tmp.getField() + tmp.getComparator() + ":param" + contador + " AND ";
            } else {
                whereCondition = whereCondition + tmp.getField() + tmp.getComparator() + ":param" + contador;
            }
            contador++;
        }
        return whereCondition;
    }

    public MapSqlParameterSource getParameters() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        int contador = 1;
        for (Condiciones tmp : condiciones) {
            params.addValue("param" + contador, tmp.getValue());
            contador++;
        }
        return params;
    }
}
