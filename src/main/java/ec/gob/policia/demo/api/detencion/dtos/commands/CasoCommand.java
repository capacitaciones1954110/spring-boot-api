package ec.gob.policia.demo.api.detencion.dtos.commands;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CasoCommand {
    @NotEmpty
    @NotNull
    private String numeroCaso;
    @NotEmpty
    @NotNull
    private String nombreCaso;
}
