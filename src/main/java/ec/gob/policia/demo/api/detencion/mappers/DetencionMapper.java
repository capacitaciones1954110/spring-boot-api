package ec.gob.policia.demo.api.detencion.mappers;

import org.springframework.stereotype.Component;

import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.entities.Detencion;

@Component
public class DetencionMapper {
    public Detencion toEntity(DetencionCommand command){
        Detencion detencion = new Detencion();
        detencion.setCedulaPersona(command.getCedula());
        detencion.setFecha(command.getFecha());
        detencion.setDetalle(command.getDetalle());
        return detencion;
    }
}
