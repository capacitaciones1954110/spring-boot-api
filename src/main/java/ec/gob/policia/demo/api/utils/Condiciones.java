package ec.gob.policia.demo.api.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Condiciones {
    private String field;
    private String comparator;
    private String value;
    private String type;
}
