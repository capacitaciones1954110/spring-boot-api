package ec.gob.policia.demo.api.detencion.servicios.usecases;

import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;

@FunctionalInterface
public interface AgregarDetencionUseCase {

    void agregar(DetencionCommand command);

}
