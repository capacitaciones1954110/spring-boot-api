package ec.gob.policia.demo.api.detencion.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import ec.gob.policia.demo.api.detencion.dtos.DetencionDto;

public class DetencionRowMapper implements RowMapper<DetencionDto> {

    @Override
    @Nullable
    public DetencionDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        DetencionDto detencionDto = new DetencionDto();
        detencionDto.setCedulaPersona(rs.getString("cedula_persona"));
        detencionDto.setDetalle(rs.getString("detalle"));
        return detencionDto;
    }
    
}
