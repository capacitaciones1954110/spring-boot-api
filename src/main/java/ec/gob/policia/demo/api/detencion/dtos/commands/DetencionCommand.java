package ec.gob.policia.demo.api.detencion.dtos.commands;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class DetencionCommand {
    @NotBlank
    @NotEmpty
    private String cedula;
    private Date fecha;
    @NotBlank
    @NotEmpty
    private String detalle;
}
