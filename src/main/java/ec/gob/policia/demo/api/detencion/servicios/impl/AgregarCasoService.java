package ec.gob.policia.demo.api.detencion.servicios.impl;

import org.springframework.stereotype.Service;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;
import ec.gob.policia.demo.api.detencion.mappers.CasoMapper;
import ec.gob.policia.demo.api.detencion.repositories.CasoRepository;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarCasoUseCase;

@Service
public class AgregarCasoService implements AgregarCasoUseCase{

    private final CasoRepository casoRepository;
    private final CasoMapper casoMapper;


    public AgregarCasoService(CasoRepository casoRepository, CasoMapper casoMapper) {
        this.casoRepository = casoRepository;
        this.casoMapper = casoMapper;
    }

    @Override
    public void agregarCaso(CasoCommand command) {
        casoRepository.save(casoMapper.toEntity(command));
    }
    
}
