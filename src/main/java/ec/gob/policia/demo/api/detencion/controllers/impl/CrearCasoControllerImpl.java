package ec.gob.policia.demo.api.detencion.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import ec.gob.policia.demo.api.detencion.controllers.CrearCasoController;
import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarCasoUseCase;
import ec.gob.policia.demo.api.utils.ResponseGenerico;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CrearCasoControllerImpl implements CrearCasoController{

    private final AgregarCasoUseCase agregarCasoUseCase;

    public CrearCasoControllerImpl(AgregarCasoUseCase agregarCasoUseCase) {
        this.agregarCasoUseCase = agregarCasoUseCase;
    }

    @Override
    public ResponseEntity<ResponseGenerico> post(@Valid CasoCommand command) {
        log.info("Agregando Caso {}", command);
        ResponseGenerico responseGenerico = new ResponseGenerico();
        agregarCasoUseCase.agregarCaso(command);
        responseGenerico.setCodigo("200");
        return ResponseEntity.ok(responseGenerico);
    }
    
}
