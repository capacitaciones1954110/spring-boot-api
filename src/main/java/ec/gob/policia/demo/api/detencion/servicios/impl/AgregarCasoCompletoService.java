package ec.gob.policia.demo.api.detencion.servicios.impl;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCompletoCommand;
import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.entities.Caso;
import ec.gob.policia.demo.api.detencion.entities.Detencion;
import ec.gob.policia.demo.api.detencion.mappers.CasoMapper;
import ec.gob.policia.demo.api.detencion.mappers.CasoStructMapper;
import ec.gob.policia.demo.api.detencion.mappers.DetencionMapper;
import ec.gob.policia.demo.api.detencion.repositories.CasoRepository;
import ec.gob.policia.demo.api.detencion.repositories.DetencionRepository;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarCasoCompletoUseCase;
import jakarta.transaction.Transactional;

@Service
public class AgregarCasoCompletoService implements AgregarCasoCompletoUseCase{
    @Autowired
    private  CasoRepository casoRepository;
    @Autowired
    private  DetencionRepository detencionRepository;
    @Autowired
    private  CasoMapper casoMapper;
    @Autowired
    private  DetencionMapper detencionMapper;
    @Autowired
    private  CasoStructMapper casoStructMapper;

    




    @Override
    @Transactional
    public void agregarCasoCompleto(CasoCompletoCommand command) throws Exception{
       //Caso caso = casoMapper.toEntity(command.getCaso());
       Caso caso = casoStructMapper.toEntity(command.getCaso());
       Caso casoPersist = casoRepository.save(caso);

       int contador = 0;
       
       for(DetencionCommand tmp: command.getDetenciones()){
            if(contador==1){
                throw new RuntimeException("Error");
            }
            Detencion detencion = detencionMapper.toEntity(tmp);
            detencion.setCaso(casoPersist);
            detencionRepository.save(detencion);
            contador++;
       }
    }
}
