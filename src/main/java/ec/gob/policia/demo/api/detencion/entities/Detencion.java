package ec.gob.policia.demo.api.detencion.entities;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
public class Detencion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String cedulaPersona;
    private Date fecha;
    private String detalle;

    @ManyToOne
    @JoinColumn(name = "caso")
    private Caso caso;
    
}
