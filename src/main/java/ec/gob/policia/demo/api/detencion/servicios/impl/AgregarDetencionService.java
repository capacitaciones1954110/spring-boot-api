package ec.gob.policia.demo.api.detencion.servicios.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.entities.Detencion;
import ec.gob.policia.demo.api.detencion.mappers.DetencionMapper;
import ec.gob.policia.demo.api.detencion.repositories.DetencionRepository;
import ec.gob.policia.demo.api.detencion.servicios.usecases.AgregarDetencionUseCase;

@Service
public class AgregarDetencionService implements AgregarDetencionUseCase {

    @Autowired
    private DetencionRepository detencionRepository;
    @Autowired
    private DetencionMapper detencionMapper;

    @Override
    public void agregar(DetencionCommand command) {
        System.out.println("Logica de metodo "+ command.toString());
        Detencion detencion = detencionMapper.toEntity(command);
        System.out.println(detencion.toString());
        detencionRepository.save(detencion);
    }
    
}
