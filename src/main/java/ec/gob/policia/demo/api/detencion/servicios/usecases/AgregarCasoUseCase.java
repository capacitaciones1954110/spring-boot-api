package ec.gob.policia.demo.api.detencion.servicios.usecases;

import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;

@FunctionalInterface
public interface AgregarCasoUseCase {

    void agregarCaso(CasoCommand command);
    
}
