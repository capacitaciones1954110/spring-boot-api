package ec.gob.policia.demo.api.detencion.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;
import ec.gob.policia.demo.api.utils.ResponseGenerico;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

public interface CrearCasoController {

     @Tag(name = "Endpoitn de prueba", description =
            "REST API for demo.")

    @Operation(
            summary =
                    "Metodo de prueba para probar la conectividad del API",
            description =
                    "Descripcion del servicio")

    @PostMapping("/v1/casos")
    ResponseEntity<ResponseGenerico> post(@Valid @RequestBody CasoCommand command);

}
