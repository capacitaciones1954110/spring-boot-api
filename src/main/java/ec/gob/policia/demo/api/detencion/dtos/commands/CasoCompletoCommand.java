package ec.gob.policia.demo.api.detencion.dtos.commands;



import java.util.List;

import lombok.Data;

@Data
public class CasoCompletoCommand {
    private CasoCommand caso;
    private List<DetencionCommand> detenciones;
}
    
