package ec.gob.policia.demo.api.detencion.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ec.gob.policia.demo.api.detencion.dtos.commands.CasoCommand;
import ec.gob.policia.demo.api.detencion.entities.Caso;

@Mapper(componentModel = "spring")
public interface CasoStructMapper {

    CasoStructMapper INSTANCE = Mappers.getMapper( CasoStructMapper.class ); 

    Caso toEntity(CasoCommand command);
}
