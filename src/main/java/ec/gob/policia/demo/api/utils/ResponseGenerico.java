package ec.gob.policia.demo.api.utils;

import lombok.Data;

@Data
public class ResponseGenerico {
    private Object data;
    private String mensaje;
    private String codigo;
}
