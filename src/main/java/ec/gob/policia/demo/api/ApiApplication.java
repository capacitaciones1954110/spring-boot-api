package ec.gob.policia.demo.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ec.gob.policia.demo.api.detencion.daos.DentencionDao;
import ec.gob.policia.demo.api.detencion.dtos.DetencionDto;
import ec.gob.policia.demo.api.detencion.dtos.commands.DetencionCommand;
import ec.gob.policia.demo.api.detencion.entities.Detencion;
import ec.gob.policia.demo.api.detencion.repositories.DetencionRepository;
import ec.gob.policia.demo.api.detencion.servicios.impl.AgregarDetencionService;
import ec.gob.policia.demo.api.utils.Condiciones;
import ec.gob.policia.demo.api.utils.Criteria;
import jakarta.annotation.PostConstruct;

@SpringBootApplication
public class ApiApplication {

	@Autowired
	private DentencionDao detencionDao;
	@Autowired
	private DetencionRepository detencionRepository;
	@Autowired
	private AgregarDetencionService agregarDetencionService;

	public static void main(String[] args) {

		SpringApplication.run(ApiApplication.class, args);
	}

	@PostConstruct
	public void init() {
		List<DetencionDto> detenciones = detencionDao.getAll();
		System.out.println("Detenciones: " + detenciones);

		List<DetencionDto> detencionesCedula = detencionDao.getByCedula("1715310809");
		System.out.println("Detenciones Cedula: " + detencionesCedula);

		List<DetencionDto> detencionesPs = detencionDao.getByCedulaPs("1715310809");
		System.out.println("Detenciones Preparestament: " + detencionesPs);

		List<DetencionDto> detencionesNamedParameter = detencionDao.getByCedulaNamedParemeter("1715310809");
		System.out.println("Detenciones Named Parameter: " + detencionesNamedParameter);

		List<Detencion> detencionesEnitdades = detencionRepository.findByCedulaPersona("1715310809");
		System.out.println("Detencion entidad: " + detencionesEnitdades);

		Detencion detencionId = detencionRepository.getById(1);
		System.out.println("Detencion por id: " + detencionId);

		Criteria criteria = new Criteria();
		List<Condiciones> condiciones = new ArrayList<>();
		condiciones.add(new Condiciones("cedula_persona", "=", "1715310809", null));
		condiciones.add(new Condiciones("detalle", "=", "detalle", null));
		criteria.setCondiciones(condiciones);
		System.out.println(criteria);

		DetencionCommand command = new DetencionCommand(
				"232334343",
				new Date(),
				"Detalle en constructor");
		// command.setCedula("183726372");
		// command.setFecha(new Date());
		// command.setDetalle("Detalle detencion");

		DetencionCommand commandBuilder = DetencionCommand.builder()
				.cedula("18938373")
				.detalle("Detalle builder")
				.build();

		//agregarDetencionService.agregar(commandBuilder);

	}

}
