package ec.gob.policia.demo.api.detencion.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ec.gob.policia.demo.api.detencion.entities.Caso;


public interface CasoRepository extends JpaRepository<Caso, Integer>{
    Caso findByNumeroCaso(String numeroCaso);
}
